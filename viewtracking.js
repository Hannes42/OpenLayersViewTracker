function bounds_screen() {
  // returns the coordinates of the four corners of the screen
  // ! regardless of rotation !
  
  // from https://stackoverflow.com/a/11744120/4828720
  var w = window,
      d = document,
      e = d.documentElement,
      g = d.getElementsByTagName('body')[0],
      x = w.innerWidth || e.clientWidth || g.clientWidth,
      y = w.innerHeight|| e.clientHeight|| g.clientHeight;
  var topleft =     map.getCoordinateFromPixel([0,0]);
  var topright =    map.getCoordinateFromPixel([x,0]);
  var bottomleft =  map.getCoordinateFromPixel([0,y]);
  var bottomright = map.getCoordinateFromPixel([x,y]);
  //console.log(bottomright[0]);
  //console.log(typeof(bottomright[0]));
  
  return [topleft, topright, bottomright, bottomleft, topleft];
}

function bounds_wgs84(points) {
  // converts coordinates from 3857 to wgs84
  [topleft, topright, bottomright, bottomleft, topleft] = points
  topleft_wgs84 = 	  ol.proj.transform(topleft, 	   'EPSG:3857', 'EPSG:4326');
  topright_wgs84 =    ol.proj.transform(topright,    'EPSG:3857', 'EPSG:4326');
  bottomleft_wgs84 =  ol.proj.transform(bottomleft,  'EPSG:3857', 'EPSG:4326');
  bottomright_wgs84 = ol.proj.transform(bottomright, 'EPSG:3857', 'EPSG:4326');
  //console.log(bottomright_wgs84[0]);
  //console.log(typeof(bottomright_wgs84[0]));
  return [topleft_wgs84, topright_wgs84, bottomright_wgs84, bottomleft_wgs84, topleft_wgs84];
}

function show_bounds() {
  // visually show what the user looked at
  
  // https://gis.stackexchange.com/questions/192112/openlayers-3-different-projection-geojson-view
  var features = (new ol.format.GeoJSON()).readFeatures(
      allbounds_asGeoJSON(allbounds),
      {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'}
    );

  unionlayer.getSource().clear();
  unionlayer.getSource().addFeatures(features);

  show_boundscount();
};

function polygon_from_bounds(bounds) {  
  // oh dear... https://gist.github.com/unicolet/2818482
  coordinates = []
  for (point of bounds) {
    c = new jsts.geom.Coordinate(point[0], point[1])
    coordinates.push(c)
  }
  
  shell = geometryFactory.createLinearRing(coordinates);
  polygon = geometryFactory.createPolygon(shell);
  
  return polygon
}

function allbounds_union(allbounds) {
  // create a geometric union of all viewed bounds
  all_polygons = [];
  
  for (const bounds of allbounds) {
    polygon = polygon_from_bounds(JSON.parse(bounds));
    //console.log(polygon.toText());
    all_polygons.push(polygon);
  }

  gc = geometryFactory.createGeometryCollection(all_polygons);
  return gc.union()
}

function show_boundscount() {
  // size if set, length if array :B
	document.getElementById('boundscount').innerHTML = allbounds.size || allbounds.length;
}

function clear_bounds() {
  unionlayer.getSource().clear();
	allbounds = [];
	show_boundscount();
}

function bounds_asGeoJSON(bounds) {
  timestamp = bounds[0];
  polygon = bounds[1];
  writer = new jsts.io.GeoJSONWriter();  // inefficient as fuck to create a new one for each conversion but who cares
  geometry = writer.write(polygon);
  feature = {
    "type": "Feature",
    "geometry": geometry,
    "properties": {
      "timestamp": timestamp
    }
  };

  return feature;
}

function allbounds_asGeoJSON(allbounds) {
  features = [];
  for (bounds of allbounds) {
    feature = bounds_asGeoJSON(bounds);
    features.push(feature);
  }

  fc = {
    "type": "FeatureCollection",
    "features": features
  };

  return fc;
}

// https://stackoverflow.com/questions/3665115/create-a-file-in-memory-for-user-to-download-not-through-server
function save(filename, data) {
    var blob = new Blob([data], {type: 'application/vnd.geo+json'});
    if(window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveBlob(blob, filename);
    }
    else{
        var elem = window.document.createElement('a');
        elem.href = window.URL.createObjectURL(blob);
        elem.download = filename;
        document.body.appendChild(elem);
        elem.click();
        document.body.removeChild(elem);
    }
}

function download_geojson() {
  save(Date.now()+".geojson", JSON.stringify(allbounds_asGeoJSON(allbounds)));
}
